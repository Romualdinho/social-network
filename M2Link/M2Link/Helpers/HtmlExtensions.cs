﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace M2Link.Helpers
{
    public static class HtmlExtensions
    {
        // Generate a styled TextBox with the specified Icon
        public static MvcHtmlString IconTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string iconName, object htmlAttributes = null)
        {
            var outerDiv = new TagBuilder("div");
            outerDiv.AddCssClass("input-group mb-2 mr-sm-2");

            var prependDiv = new TagBuilder("div");
            prependDiv.AddCssClass("input-group-prepend");

            var textDiv = new TagBuilder("div");
            textDiv.AddCssClass("input-group-text");

            var icon = new TagBuilder("i");
            icon.AddCssClass("material-icons");
            icon.SetInnerText(iconName);

            var input = helper.TextBoxFor(expression, htmlAttributes);

            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.Append(prependDiv.ToString(TagRenderMode.StartTag));
            htmlBuilder.Append(textDiv.ToString(TagRenderMode.StartTag));
            htmlBuilder.Append(icon.ToString(TagRenderMode.Normal));
            htmlBuilder.Append(textDiv.ToString(TagRenderMode.EndTag));
            htmlBuilder.Append(prependDiv.ToString(TagRenderMode.EndTag));
            htmlBuilder.Append(input.ToHtmlString());

            outerDiv.InnerHtml = htmlBuilder.ToString();

            return new MvcHtmlString(outerDiv.ToString(TagRenderMode.Normal));
        }
    }
}