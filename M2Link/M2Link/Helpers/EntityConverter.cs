﻿using M2Link.Entities;
using M2Link.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link.Helpers
{
    public static class EntityConverter
    {
        // Return a UserModel instance mapped on the user entity properties
        public static UserModel createUserModel(User user, bool preventUserCycle = false, bool preventMessageCycle = false)
        {
            return new UserModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Mail = user.Mail,
                Username = user.Username,
                Description = user.Description,
                Avatar = user.Avatar,
                Messages = preventMessageCycle ? null : user.Messages.Select(m => createMessageModel(m, preventUserCycle)).OrderByDescending(m => m.Date).ToList(),
                Followers = preventUserCycle ? null : user.Followers.Select(u => createUserModel(u, true)).ToList(),
                Followings = preventUserCycle ? null : user.Followings.Select(u => createUserModel(u, true)).ToList()
            };
        }

        // Return a MessageModel instance mapped on the message entity properties
        public static MessageModel createMessageModel(Message message, bool preventCycle = false)
        {
            return new MessageModel
            {
                Id = message.Id,
                Author = createUserModel(message.Author, preventCycle, true),
                Content = message.Content,
                Date = message.Date,
                Image = message.Image
            };
        }
    }
}