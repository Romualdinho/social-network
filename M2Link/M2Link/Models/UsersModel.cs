﻿using M2Link.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class UsersModel
    {
        public UserModel AuthenticatedUser { get; set; }
        public List<UserModel> Users { get; set; }
    }
}