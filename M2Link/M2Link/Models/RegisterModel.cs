﻿using M2Link.Entities;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace M2Link.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Le prénom est un champ obligatoire.")]
        [RegularExpression("^[ a-zA-Z-]{1,30}$", ErrorMessage = "Le prénom ne doit pas excéder 30 caractères et contenir uniquement des lettres.")]
        [DisplayName("Prénom")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Le nom est un champ obligatoire.")]
        [RegularExpression("^[ a-zA-Z-]{1,30}$", ErrorMessage = "Le nom ne doit pas excéder 30 caractères et contenir uniquement des lettres.")]
        [DisplayName("Nom")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "L'adresse mail est un champ obligatoire.")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(100, ErrorMessage = "L'adresse mail ne doit pas excéder 100 caractères.")]
        [DisplayName("Email")]
        public string Mail { get; set; }

        [Required(ErrorMessage = "Le pseudo est un champ obligatoire.")]
        [RegularExpression("^[\\da-zA-Z-_]{1,30}$", ErrorMessage = "Le pseudo ne doit pas excéder 30 caractères et contenir uniquement des lettres, chiffres ou caractères spéciaux (- ou _).")]
        [DisplayName("Pseudo")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Le mot de passe est un champ obligatoire.")]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^\\da-zA-Z])(.{8,30})$",
            ErrorMessage = "Le mot de passe doit faire entre 8 et 30 caractères, contenir des lettres, majuscules, chiffres et caractères spéciaux.")]
        [DisplayName("Mot de passe")]
        public string Password { get; set; }

        [Required(ErrorMessage = "La confirmation du mot de passe est obligatoire.")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage ="Les deux mots de passe doivent être identiques.")]
        [DisplayName("Confirmer le mot de passe")]
        public string ConfirmPassword { get; set; }
    }
}