﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace M2Link.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage ="Le pseudo est un champ obligatoire.")]
        [MaxLength(30, ErrorMessage = "Le pseudo ne doit pas excéder 30 caractères.")]
        [DisplayName("Pseudo")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Le mot de passe est un champ obligatoire.")]
        [DataType(DataType.Password)]
        [DisplayName("Mot de passe")]
        public string Password { get; set; }
    }
}