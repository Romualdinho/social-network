﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class EditProfileModel
    {
        [Required(ErrorMessage = "Le prénom est un champ obligatoire.")]
        [RegularExpression("^[ a-zA-Z-]{1,30}$", ErrorMessage = "Le prénom ne doit pas excéder 30 caractères et contenir uniquement des lettres.")]
        [DisplayName("Prénom")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Le nom est un champ obligatoire.")]
        [RegularExpression("^[ a-zA-Z-]{1,30}$", ErrorMessage = "Le nom ne doit pas excéder 30 caractères et contenir uniquement des lettres.")]
        [DisplayName("Nom")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "L'adresse mail est un champ obligatoire.")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(100, ErrorMessage = "L'adresse mail ne doit pas excéder 30 caractères.")]
        [DisplayName("Email")]
        public string Mail { get; set; }

        [Required(ErrorMessage = "Le pseudo est un champ obligatoire.")]
        [RegularExpression("^[\\da-zA-Z-_]{1,30}$", ErrorMessage = "Le pseudo ne doit pas excéder 30 caractères et contenir uniquement des lettres, chiffres ou caractères spéciaux (- ou _).")]
        [DisplayName("Pseudo")]
        public string Username { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [MaxLength(150, ErrorMessage = "La description ne doit pas excéder 150 caractères.")]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Ancien mot de passe")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^\\da-zA-Z])(.{8,30})$",
            ErrorMessage ="Le mot de passe doit faire entre 8 et 30 caractères, contenir des lettres, majuscules, chiffres et caractères spéciaux.")]
        [DisplayName("Nouveau mot de passe")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Les deux mots de passe doivent être identiques.")]
        [DisplayName("Confirmer le nouveau mot de passe")]
        public string ConfirmPassword { get; set; }
    }
}