﻿using M2Link.Entities;
using M2Link.Helpers;
using M2Link.Models;
using M2Link.Shared.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace M2Link.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        // GET : profile
        [Route("profile")]
        public ActionResult MyProfile()
        {
            // Filling the Profile form with the authenticated user information
            return RedirectToAction("GetProfile", new { u = GetAuthenticatedUser().Username });
        }

        // GET : profile/{username}
        [Route("profile/{u}", Name = "GetProfile")]
        public ActionResult GetProfile(string u)
        {
            User user = UserRepository.Instance.FindByUsername(u);

            // If the user does not exist
            if (user == null)
            {
                return View("NotFound");
            }

            return View("~/Views/Profile/Index.cshtml", EntityConverter.createUserModel(user));
        }

        // GET : profile/{username}
        [Route("profile/{u}/followings", Name = "GetProfileFollowings")]
        public ActionResult GetProfileFollowings(string u)
        {
            User user = UserRepository.Instance.FindByUsername(u);

            // If the user does not exist
            if (user == null)
            {
                return View("NotFound");
            }

            ViewBag.AuthenticatedUser = EntityConverter.createUserModel(GetAuthenticatedUser());

            return View("~/Views/Profile/Followings.cshtml", EntityConverter.createUserModel(user));
        }

        // GET : profile/{username}
        [Route("profile/{u}/followers", Name = "GetProfileFollowers")]
        public ActionResult GetProfileFollowers(string u)
        {
            User user = UserRepository.Instance.FindByUsername(u);

            // If the user does not exist
            if (user == null)
            {
                return View("NotFound");
            }

            ViewBag.AuthenticatedUser = EntityConverter.createUserModel(GetAuthenticatedUser());

            return View("~/Views/Profile/Followers.cshtml", EntityConverter.createUserModel(user));
        }

        // GET : profile/{username}/edit
        [Route("profile/{u}/edit", Name = "EditProfile")]
        public ActionResult EditProfile(string u)
        {
            User authenticatedUser = GetAuthenticatedUser();

            // If the authenticated user try to edit the profile of someone else, redirect
            if (authenticatedUser.Username.ToLower() != u.ToLower())
            {
                return RedirectToAction("GetProfile", new { u });
            }

            // Filling the edit profile form with the user information
            EditProfileModel model = new EditProfileModel()
            {
                FirstName = authenticatedUser.FirstName,
                LastName = authenticatedUser.LastName,
                Username = authenticatedUser.Username,
                Mail = authenticatedUser.Mail,
                Description = authenticatedUser.Description
            };

            return View("~/Views/Profile/Edit.cshtml", model);
        }

        // POST : profile/avatar
        [HttpPost]
        [Route("profile/avatar", Name = "EditAvatar")]
        public ActionResult EditAvatar()
        {
            User authenticatedUser = GetAuthenticatedUser();

            var file = Request.Files[0];

            if (file == null || file.ContentLength == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                var allowedExtensions = new[] { ".jpg", ".jpeg", ".png", ".gif", ".svg", ".bmp" };
            
                // If the user try to upload a file which is not a image
                if (!allowedExtensions.Contains(Path.GetExtension(file.FileName))) {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                string fileName = authenticatedUser.Id.ToString() + Path.GetExtension(file.FileName);
                string filePath = Path.Combine(Server.MapPath("~/Content/Images/Users"), fileName);

                // If the user has already an avatar, delete the old image
                if (authenticatedUser.Avatar != "no_profile_image.png")
                {
                    System.IO.File.Delete(Path.Combine(Server.MapPath("~/Content/Images/Users"), authenticatedUser.Avatar));
                }

                // Save the image
                file.SaveAs(filePath);

                // Update the image reference
                authenticatedUser.Avatar = fileName;

                // Update the database
                UserRepository.Instance.Update(authenticatedUser);

                // Send the image path for the refresh
                return Json(new { success = true, image = fileName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }


        // POST : profile/edit
        [HttpPost]
        [Route("profile/edit")]
        public ActionResult EditProfile(EditProfileModel model)
        {
            User authenticatedUser = GetAuthenticatedUser();

            // If the user tried to change is username
            if (authenticatedUser.Username != model.Username && UserRepository.Instance.FindByUsername(model.Username) != null )
            {
                ModelState.AddModelError("Username", UserRepository.USERNAME_ALREADY_EXIST_MESSAGE);
            }

            // If the user tried to change is email
            if (authenticatedUser.Mail != model.Mail && UserRepository.Instance.FindByMail(model.Mail) != null)
            {
                ModelState.AddModelError("Mail", UserRepository.MAIL_ALREADY_EXIST_MESSAGE);
            }

            // If the model is not valid, redirect on the same page with model errors
            if (!ModelState.IsValid)
            {
                return View("~/Views/Profile/Edit.cshtml", model);
            }

            // If the user try to changed his password
            if (!string.IsNullOrEmpty(model.Password))
            {
                // If the old password is incorrect
                if (string.IsNullOrEmpty(model.OldPassword) || authenticatedUser.Password != Crypto.SHA256(model.OldPassword))
                {
                    ModelState.AddModelError("OldPassword", "Le mot de passe est incorrect");
                    return View("~/Views/Profile/Edit.cshtml", model);
                }

                // Set the new password
                authenticatedUser.Password = Crypto.SHA256(model.Password);
            }

            // Setting up the new information of the user profile
            authenticatedUser.FirstName = model.FirstName.Trim();
            authenticatedUser.LastName = model.LastName.Trim();
            authenticatedUser.Username = model.Username.Trim();
            authenticatedUser.Mail = model.Mail.Trim();
            authenticatedUser.Description = model.Description.Trim();

            UserRepository.Instance.Update(authenticatedUser);
            
            // Redirect on the Profile page
            return RedirectToAction("MyProfile");
        }

        // GET : users
        [Route("users")]
        public ActionResult GetUsers(string search)
        {
            List<UserModel> users = new List<UserModel>();

            if (string.IsNullOrEmpty(search))
            {
                users = UserRepository.Instance.GetAll().Select(u => EntityConverter.createUserModel(u)).ToList();
            }
            else
            {
                users = UserRepository.Instance.FindByGlobalSearch(search).Select(u => EntityConverter.createUserModel(u)).ToList();
            }

            return View("~/Views/Users/Index.cshtml", users);
        }

        // POST : User/Follow
        [HttpPost]
        public ActionResult Follow(string username)
        {
            try
            {
                User authenticatedUser = GetAuthenticatedUser();
                User targetUser = UserRepository.Instance.FindByUsername(username);

                // If the user try to follow himself
                if (authenticatedUser.Id.Equals(targetUser.Id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                // Prevent duplicates
                if (!authenticatedUser.Followings.Contains(targetUser))
                {
                    // Update the relation between the two users
                    authenticatedUser.Followings.Add(targetUser);

                    // Update the database
                    UserRepository.Instance.Update(authenticatedUser);
                }

                // Prevent duplicates
                if (!targetUser.Followers.Contains(authenticatedUser))
                {
                    // Update the relation between the two users
                    targetUser.Followers.Add(authenticatedUser);

                    // Update the database
                    UserRepository.Instance.Update(targetUser);
                }

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST : User/Unfollow
        [HttpPost]
        public ActionResult Unfollow(string username)
        {
            try
            {
                User authenticatedUser = GetAuthenticatedUser();
                User targetUser = UserRepository.Instance.FindByUsername(username);

                // If the user try to unfollow himself
                if (authenticatedUser.Id.Equals(targetUser.Id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                // Update the relation between the two users
                authenticatedUser.Followings.Remove(targetUser);
                targetUser.Followers.Remove(authenticatedUser);

                // Update the database
                UserRepository.Instance.Update(authenticatedUser);
                UserRepository.Instance.Update(targetUser);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST : User/Message
        [HttpPost]
        public ActionResult PostMessage(string message, HttpPostedFileBase file)
        {
            User authenticatedUser = GetAuthenticatedUser();

            // Create a new message
            Message m = new Message()
            {
                Author = authenticatedUser,
                Content = message,
                Date = DateTime.Now
            };

            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png", ".gif", ".svg", ".bmp" };
            
            // Check if the message contains an image
            if (file != null && file.ContentLength > 0 && allowedExtensions.Contains(Path.GetExtension(file.FileName)))
            {
                string fileName = m.Id.ToString() + Path.GetExtension(file.FileName);
                string filePath = Path.Combine(Server.MapPath("~/Content/Images/Messages"), fileName);

                // Save the image
                file.SaveAs(filePath);

                // Put the image path reference
                m.Image = fileName;
            }

            MessageRepository.Instance.Add(m);

            // Return the created message with HTML format
            return PartialView("_Message", EntityConverter.createMessageModel(m));
        }

        // POST : User/Delete
        [HttpDelete]
        public ActionResult DeleteMessage(string id)
        {
            try
            {
                User authenticatedUser = GetAuthenticatedUser();

                Message message = MessageRepository.Instance.FindById(Guid.Parse(id));

                // If the authenticated user is not the author of this message
                if (!message.Author.Id.Equals(authenticatedUser.Id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                // Delete the message from the database
                MessageRepository.Instance.Remove(message);

                // If the message contains an image, delete it
                if (!string.IsNullOrEmpty(message.Image))
                {
                    System.IO.File.Delete(Path.Combine(Server.MapPath("~/Content/Images/Messages"), message.Image));
                }

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        private User GetAuthenticatedUser()
        {
            return UserRepository.Instance.FindById(Guid.Parse(User.Identity.Name));
        }
    }
}