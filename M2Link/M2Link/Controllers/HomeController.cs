﻿using M2Link.Entities;
using M2Link.Helpers;
using M2Link.Models;
using M2Link.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace M2Link.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        // GET : Home/Index
        public ActionResult Index()
        {
            User user = UserRepository.Instance.FindById(Guid.Parse(User.Identity.Name));
            return View("Index", MessageRepository.Instance.GetFeed(user).Select(m => EntityConverter.createMessageModel(m)).ToList());
        }
    }
}