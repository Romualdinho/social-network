﻿using M2Link.Entities;
using M2Link.Models;
using System;
using System.Web.Helpers;
using System.Web.Mvc;

namespace M2Link.Controllers
{
    public class RegisterController : Controller
    {
        // GET : Register/Form
        public ActionResult Form()
        {
            // If the user is already authenticated, redirection on Home page
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("/");
            }

            return View(new RegisterModel());
        }

        // POST : Register/Form
        [HttpPost]
        public ActionResult Form(RegisterModel model)
        {
            // If the model is not valid, redirect on the same page with errors
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                // Create the new user from model data
                UserRepository.Instance.Add(new User
                {
                    FirstName = model.FirstName.Trim(),
                    LastName = model.LastName.Trim(),
                    Mail = model.Mail.Trim(),
                    Username = model.Username.Trim(),
                    Password = Crypto.SHA256(model.Password)
                });
            }
            // If an error occured during the creation of the user account
            catch (ApplicationException e)
            {
                // Show the approriate error
                if (e.Message == UserRepository.MAIL_ALREADY_EXIST_MESSAGE)
                {
                    ModelState.AddModelError("Mail", e.Message);
                }
                else if (e.Message == UserRepository.USERNAME_ALREADY_EXIST_MESSAGE)
                {
                    ModelState.AddModelError("Username", e.Message);
                }

                // Redirect on the same page with model errors
                return View(model);
            }

            // Redirect on the Home page
            return Redirect("/");
        }
    }
}