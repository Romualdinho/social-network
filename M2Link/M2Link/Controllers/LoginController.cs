﻿using M2Link.Entities;
using M2Link.Models;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace M2Link.Controllers
{
    public class LoginController : Controller
    {
        // GET : Login/Form
        public ActionResult Form()
        {
            // If the user is already authenticated, redirection on Home page
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("/");
            }

            return View(new LoginModel());
        }

        // POST : Login/Form
        [HttpPost]
        public ActionResult Form(LoginModel model)
        {
            // If the model is not valid, redirect on the same page with model errors
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            User user = UserRepository.Instance.FindByUsername(model.Username);

            // If no user with specified credentials were found
            if (user == null)
            {
                ModelState.AddModelError("Username", "L'utilisateur n'existe pas");
                return View(model);
            }

            // If the hashed password does not match with the hash of the specified password
            if (user.Password != Crypto.SHA256(model.Password))
            {
                ModelState.AddModelError("Password", "Le mot de passe est incorrect");
                return View(model);
            }

            // If credentials are correct, setting up a authentication cookie
            FormsAuthentication.SetAuthCookie(user.Id.ToString(), true);
            
            // Redirect on the Home page
            return Redirect("/");
        }

        // GET: Login/Logout
        public void Logout()
        {
            // Logout from the application and redirect to the login page
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
}