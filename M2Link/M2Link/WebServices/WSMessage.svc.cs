﻿using M2Link.Entities;
using M2Link.Helpers;
using M2Link.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "WSMessage" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez WSMessage.svc ou WSMessage.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class WSMessage : IWSMessage
    {
        public IEnumerable<MessageModel> GetFeed(string accessToken)
        {
            // Check if the user is authenticated
            User authenticatedUser = WSLogin.GetAuthenticatedUser(accessToken);

            return MessageRepository.Instance.GetFeed(authenticatedUser).Select(m => EntityConverter.createMessageModel(m, true));
        }

        public MessageModel PostMessage(string accessToken, string content)
        {
            // Check if the user is authenticated
            User authenticatedUser = WSLogin.GetAuthenticatedUser(accessToken);

            // Create the message
            Message message = new Message()
            {
                Author = authenticatedUser,
                Content = content,
                Date = DateTime.Now
            };

            MessageRepository.Instance.Add(message);

            return EntityConverter.createMessageModel(message);
        }

        public void DeleteMessage(string accessToken, string id)
        {
            // Check if the user is authenticated
            User authenticatedUser = WSLogin.GetAuthenticatedUser(accessToken);

            Message message = MessageRepository.Instance.FindById(Guid.Parse(id));

            // If the authenticated user is not the author of this message
            if (!message.Author.Id.Equals(authenticatedUser.Id))
            {
                throw new FaultException("Vous ne pouvez pas supprimer un message dont vous n'êtes pas l'auteur");
            }

            MessageRepository.Instance.Remove(message);
        }
    }
}
