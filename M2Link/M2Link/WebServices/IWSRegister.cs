﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IWSRegister" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IWSRegister
    {
        [OperationContract]
        void Register(string firstName,
            string lastName,
            string email,
            string username,
            string password,
            string confirmPassword);
    }
}
