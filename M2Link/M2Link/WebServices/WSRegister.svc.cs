﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Helpers;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "WSRegister" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez WSRegister.svc ou WSRegister.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class WSRegister : IWSRegister
    {
        public void Register(string firstName, string lastName, string email, string username, string password, string confirmPassword)
        {
            if (password != confirmPassword)
            {
                throw new FaultException("La confirmation du mot de passe est incorrecte");
            }

            try
            {
                // Create the new user from model data
                UserRepository.Instance.Add(new User
                {
                    FirstName = firstName.Trim(),
                    LastName = lastName.Trim(),
                    Mail = email.Trim(),
                    Username = username.Trim(),
                    Password = Crypto.SHA256(password)
                });
            }

            // If an error occured during the creation of the user account
            catch (ApplicationException e)
            {
                throw new FaultException(e.Message);
            }
        }
    }
}
