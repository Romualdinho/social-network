﻿using M2Link.Entities;
using M2Link.Helpers;
using M2Link.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Helpers;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "WSUser" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez WSUser.svc ou WSUser.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class WSUser : IWSUser
    {
        public UserModel GetProfile(string accessToken, string username = "")
        {
            // Check if the user is authenticated
            User authenticatedUser = WSLogin.GetAuthenticatedUser(accessToken);

            // If there is no username specified, return the profile of the authenticated user
            if (string.IsNullOrEmpty(username))
            {
                return EntityConverter.createUserModel(authenticatedUser);
            }

            return EntityConverter.createUserModel(UserRepository.Instance.FindByUsername(username));
        }

        public UserModel EditProfile(string accessToken, string firstName, string lastName, string email, string username, string description, string oldPassword, string password, string confirmPassword)
        {
            // Check if the user is authenticated
            User authenticatedUser = WSLogin.GetAuthenticatedUser(accessToken);

            // If the user tried to change is username
            if (authenticatedUser.Username != username && UserRepository.Instance.FindByUsername(username) != null)
            {
                throw new FaultException(UserRepository.USERNAME_ALREADY_EXIST_MESSAGE);
            }

            // If the user tried to change is email
            if (authenticatedUser.Mail != email && UserRepository.Instance.FindByMail(email) != null)
            {
                throw new FaultException(UserRepository.MAIL_ALREADY_EXIST_MESSAGE);
            }

            // If the user try to changed his password
            if (!string.IsNullOrEmpty(password))
            {
                // If the old password is incorrect
                if (string.IsNullOrEmpty(oldPassword) || authenticatedUser.Password != Crypto.SHA256(oldPassword))
                {
                    throw new FaultException("Le mot de passe est incorrect");
                }

                // Set the new password
                authenticatedUser.Password = Crypto.SHA256(password);
            }

            // Update all fields
            authenticatedUser.FirstName = firstName.Trim();
            authenticatedUser.LastName = lastName.Trim();
            authenticatedUser.Username = username.Trim();
            authenticatedUser.Mail = email.Trim();
            authenticatedUser.Description = description.Trim();

            // Update the database
            UserRepository.Instance.Update(authenticatedUser);

            return EntityConverter.createUserModel(authenticatedUser);

        }

        public IEnumerable<UserModel> GetAll(string accessToken, string search = "")
        {
            // Check if the user is authenticated
            WSLogin.GetAuthenticatedUser(accessToken);

            List<UserModel> users = new List<UserModel>();

            if (string.IsNullOrEmpty(search))
            {
                users = UserRepository.Instance.GetAll().Select(u => EntityConverter.createUserModel(u)).ToList();
            }
            else
            {
                users = UserRepository.Instance.FindByGlobalSearch(search).Select(u => EntityConverter.createUserModel(u)).ToList();
            }

            return users;
        }

        public void Follow(string accessToken, string username)
        {
            // Check if the user is authenticated
            User authenticatedUser = WSLogin.GetAuthenticatedUser(accessToken);

            User targetUser = UserRepository.Instance.FindByUsername(username);

            if (targetUser == null)
            {
                throw new FaultException("L'utilisateur n'existe pas");
            }

            if (authenticatedUser.Id.Equals(targetUser.Id))
            {
                throw new FaultException("Vous ne pouvez pas vous suivre vous-même");
            }

            // Update relation between the two users and the database
            if (!authenticatedUser.Followings.Contains(targetUser))
            {
                authenticatedUser.Followings.Add(targetUser);
                UserRepository.Instance.Update(authenticatedUser);
            }

            if (!targetUser.Followers.Contains(authenticatedUser))
            {
                targetUser.Followers.Add(authenticatedUser);
                UserRepository.Instance.Update(targetUser);
            }
        }

        public void Unfollow(string accessToken, string username)
        {
            // Check if the user is authenticated
            User authenticatedUser = WSLogin.GetAuthenticatedUser(accessToken);

            User targetUser = UserRepository.Instance.FindByUsername(username);

            if (targetUser == null)
            {
                throw new FaultException("L'utilisateur n'existe pas");
            }

            if (authenticatedUser.Id.Equals(targetUser.Id))
            {
                throw new FaultException("Vous ne pouvez pas vous suivre vous-même");
            }

            // Update relation between the two users
            authenticatedUser.Followings.Remove(targetUser);
            targetUser.Followers.Remove(authenticatedUser);

            // Update the database
            UserRepository.Instance.Update(authenticatedUser);
            UserRepository.Instance.Update(targetUser);
        }
    }
}
