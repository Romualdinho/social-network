﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Helpers;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "WSLogin" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez WSLogin.svc ou WSLogin.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class WSLogin : IWSLogin
    {
        private static Dictionary<string, User> authenticatedUsers = new Dictionary<string, User>();

        public string Validate(string username, string password)
        {
            User user = UserRepository.Instance.FindByUsername(username);

            // If the user does not exist or the password is incorrect
            if (user == null || Crypto.SHA256(password) != user.Password)
            {
                return null;
            }

            // Create a access token for this user
            string accessToken = Guid.NewGuid().ToString();
            authenticatedUsers.Add(accessToken, user);

            // Return the access token
            return accessToken;
        }


        public static User GetAuthenticatedUser(string accessToken)
        {
            // If the access token is incorrect
            if (!authenticatedUsers.ContainsKey(accessToken))
            {
                throw new FaultException("Accès refusé");
            }

            return authenticatedUsers[accessToken];
        }
    }
}
