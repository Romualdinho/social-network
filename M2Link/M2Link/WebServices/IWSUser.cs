﻿using M2Link.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace M2Link.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IWSUser" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IWSUser
    {
        [OperationContract]
        UserModel GetProfile(string accessToken, string username = "");

        [OperationContract]
        UserModel EditProfile(string accessToken,
            string firstName,
            string lastName,
            string email,
            string username,
            string description,
            string oldPassword,
            string password,
            string confirmPassword);

        [OperationContract]
        IEnumerable<UserModel> GetAll(string accessToken, string search = "");

        [OperationContract]
        void Follow(string accessToken, string username);

        [OperationContract]
        void Unfollow(string accessToken, string username);
    }
}
