﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace M2Link.Entities
{
    public class Message
    {
        [KeyAttribute]
        public Guid Id { get; set; }
        public virtual User Author { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public string Image { get; set; }

        public Message()
        {
            Id = Guid.NewGuid();
        }
    }
}