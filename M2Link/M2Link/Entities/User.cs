﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace M2Link.Entities
{
    public class User
    {
        [KeyAttribute]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mail { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<User> Followers { get; set; }
        public virtual ICollection<User> Followings { get; set; }
       
        public User()
        {
            Id = Guid.NewGuid();
            Description = "";
            Avatar = "no_profile_image.png";
            Messages = new List<Message>();
            Followers = new List<User>();
            Followings = new List<User>();
        }
    }
}