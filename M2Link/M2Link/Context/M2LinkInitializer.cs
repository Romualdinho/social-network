﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace M2Link.Context
{
    public class M2LinkInitializer : DropCreateDatabaseIfModelChanges<M2LinkContext>
    {
        protected override void Seed(M2LinkContext context)
        {

            User ada = new User
            {
                Id = Guid.Parse("ef7a109e-f0e8-4902-847f-3cb394c30c2c"),
                FirstName = "Ada",
                LastName = "Lovelace",
                Mail = "ada.lovelace@gmail.com",
                Username = "Ada",
                Description = "Pionnière de la science informatique, a réalisé le premier programme informatique",
                Avatar = "ef7a109e-f0e8-4902-847f-3cb394c30c2c.jpg",
                Password = Crypto.SHA256("P@sSw0rD")
            };

            User alan = new User
            {
                Id = Guid.Parse("076f8f54-d822-49cf-854a-fc2569a2e97a"),
                FirstName = "Alan",
                LastName = "Turing",
                Mail = "alan.turing@gmail.com",
                Username = "AlanT",
                Description = "Mathématicien et cryptologue britannique, précurseur de l'informatique",
                Avatar = "076f8f54-d822-49cf-854a-fc2569a2e97a.jpg",
                Password = Crypto.SHA256("P@sSw0rD")
            };

            User charles = new User
            {
                Id = Guid.Parse("253dbd83-5fea-4389-8039-78d3ff079ea1"),
                FirstName = "Charles",
                LastName = "Babbage",
                Mail = "charles.babbage@gmail.com",
                Username = "CharlesBB",
                Description = "Inventeur de la machine à calculer",
                Avatar = "253dbd83-5fea-4389-8039-78d3ff079ea1.jpg",
                Password = Crypto.SHA256("P@sSw0rD")
            };

            User steve = new User
            {
                Id = Guid.Parse("2510620f-9866-42d6-965b-471949af2d48"),
                FirstName = "Steve",
                LastName = "Jobs",
                Mail = "steve.jobs@apple.com",
                Username = "SteveJobs",
                Description = "Fondateur de Apple",
                Avatar = "2510620f-9866-42d6-965b-471949af2d48.jpg",
                Password = Crypto.SHA256("P@sSw0rD")
            };

            User bill = new User
            {
                Id = Guid.Parse("b13050a2-31a8-42de-9c84-451b551136aa"),
                FirstName = "Bill",
                LastName = "Gates",
                Mail = "bill.gates@microsoft.com",
                Username = "TheBill",
                Description = "Fondateur de Microsoft",
                Avatar = "b13050a2-31a8-42de-9c84-451b551136aa.jpg",
                Password = Crypto.SHA256("P@sSw0rD")
            };

            // Setting up users...
            context.Users.Add(ada);
            context.Users.Add(alan);
            context.Users.Add(charles);
            context.Users.Add(steve);
            context.Users.Add(bill);

            // Setting up relations between users...
            ada.Followings.Add(alan);
            ada.Followings.Add(charles);
            ada.Followings.Add(steve);
            ada.Followings.Add(bill);

            alan.Followings.Add(ada);
            alan.Followings.Add(charles);
            alan.Followings.Add(charles);

            charles.Followings.Add(ada);
            charles.Followings.Add(alan);
            charles.Followings.Add(steve);

            steve.Followings.Add(bill);
            steve.Followings.Add(alan);

            bill.Followings.Add(steve);

            // Setting up messages...

            context.Messages.Add(new Message
            {
                Author = ada,
                Content = "La machine analytique tisse des motifs algébriques tout comme le métier Jacquard tisse des fleurs et des feuilles.",
                Date = DateTime.Parse("1850-02-04 08:12:00"),
            });

            context.Messages.Add(new Message
            {
                Id = Guid.Parse("78d408e7-ae33-43a7-a13b-6676ccfd58cd"),
                Author = ada,
                Content = "La machine analytique n'a pas de prétention à donner naissance à quoi que ce soit. Elle peut faire ce que nous savons lui apprendre à faire. Elle peut suivre l'analyse, mais elle n'a pas le pouvoir d'anticiper des relations analytiques ou des vérités.",
                Date = DateTime.Parse("1852-02-18 12:47:00"),
                Image = "78d408e7-ae33-43a7-a13b-6676ccfd58cd.jpg"
            });

            context.Messages.Add(new Message
            {
                Author = charles,
                Content = "À deux reprises, on m’a demandé, \"Priez, M. Babbage, si vous mettez dans la machine les chiffres faux , les bonnes réponses sortiront-elles?\" Justement je ne suis pas en mesure d’appréhender le genre de confusion des idées qui pourraient provoquer une telle question.",
                Date = DateTime.Parse("1860-11-10 20:15:00")
            });

            context.Messages.Add(new Message
            {
                Author = charles,
                Content = "Les erreurs d'utilisation de données inappropriées sont bien inférieurs à celles n'utilisant aucune donnée.",
                Date = DateTime.Parse("1865-06-07 13:35:00")
            });

            context.Messages.Add(new Message
            {
                Author = alan,
                Content = "Les tentatives de création de machines pensantes nous seront d'une grande aide pour découvrir comment nous pensons nous-mêmes.",
                Date = DateTime.Parse("1951-05-15 17:20:00")
            });

            context.Messages.Add(new Message
            {
                Author = alan,
                Content = "Je crois que dans une cinquantaine d’années il sera possible de programmer des ordinateurs, avec une capacité de mémoire d’à peu près 10⁹.",
                Date = DateTime.Parse("1950-02-18 06:42:00")
            });

            context.Messages.Add(new Message
            {
                Id = Guid.Parse("decda572-02f5-45d8-890e-e67941be03e2"),
                Author = alan,
                Content = "Nous avons enfin réussi à casser le code de la marine nazi!",
                Date = DateTime.Parse("1942-11-24 23:57:00"),
                Image = "decda572-02f5-45d8-890e-e67941be03e2.jpeg"
            });

            context.Messages.Add(new Message
            {
                Author = bill,
                Content = "Célébrer le succès, c’est bien, mais il est plus important de tirer les leçons de l’échec.",
                Date = DateTime.Parse("1993-10-13 12:34:00")
            });

            context.Messages.Add(new Message
            {
                Id = Guid.Parse("c5c8691b-62ae-4ca5-b519-75f58517293a"),
                Author = bill,
                Content = "Je choisis toujours une personne fainéante pour effectuer un travail difficile. Car je sais qu’elle trouvera un moyen facile de le faire.",
                Date = DateTime.Parse("1994-07-20 16:13:00"),
                Image = "c5c8691b-62ae-4ca5-b519-75f58517293a.jpg"
            });

            context.Messages.Add(new Message
            {
                Id = Guid.Parse("873bedcd-a758-496e-9522-c7bcd40ede39"),
                Author = bill,
                Content = "J’ai échoué à quelques examens, mais un de mes amis les a tous réussis. Maintenant, il est ingénieur chez Microsoft. Moi, je suis le dirigeant de Microsoft.",
                Date = DateTime.Parse("2005-10-13 12:34:00"),
                Image = "873bedcd-a758-496e-9522-c7bcd40ede39.jpg"
            });

            context.Messages.Add(new Message
            {
                Author = bill,
                Content = "Si vous pensez que votre professeur est dur avec vous, attendez d’avoir un patron.",
                Date = DateTime.Parse("2008-10-13 12:34:00")
            });

            context.Messages.Add(new Message
            {
                Author = steve,
                Content = "Je suis convaincu que la moitié qui sépare les entrepreneurs qui réussissent de ceux qui échouent est purement la persévérance.",
                Date = DateTime.Parse("1996-07-10 14:03:00")
            });

            context.Messages.Add(new Message
            {
                Id = Guid.Parse("ac4e580d-672a-400b-8f4f-f67fb168b19c"),
                Author = steve,
                Content = "L'iMac c'est l'ordinateur de l'année prochaine pour 1299$ pas l'ordinateur de l'année dernière pour 999$.",
                Date = DateTime.Parse("1998-08-01 14:00:00"),
                Image = "ac4e580d-672a-400b-8f4f-f67fb168b19c.jpg"
            });

            context.Messages.Add(new Message
            {
                Author = steve,
                Content = "Ce n'est pas le boulot des consommateurs de savoir ce qu'ils veulent.",
                Date = DateTime.Parse("2005-12-20 19:54:00")
            });

            context.Messages.Add(new Message
            {
                Id = Guid.Parse("b53114ae-61d1-4eac-a8a0-46c1d78d91f2"),
                Author = steve,
                Content = "L’innovation est ce qui distingue un meneur d’un suiveur.",
                Date = DateTime.Parse("2007-06-25 19:30:00"),
                Image = "b53114ae-61d1-4eac-a8a0-46c1d78d91f2.jpg"
            });
        }
    }
}