﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace M2Link.Context
{
    public class M2LinkContext : DbContext
    {
        public static readonly M2LinkContext Instance = new M2LinkContext();

        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }

        private M2LinkContext() : base("M2LinkContext")
        {
        }
    }
}