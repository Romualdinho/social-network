﻿using M2Link.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace M2Link.Entities
{
    // Singleton used to manage User Entities
    public class UserRepository
    {
        public static string MAIL_ALREADY_EXIST_MESSAGE = "Cette adresse mail est déjà utilisée par un autre utilisateur";
        public static string USERNAME_ALREADY_EXIST_MESSAGE = "Ce pseudo est déjà utilisé par un autre utilisateur";

        public static readonly UserRepository Instance = new UserRepository(M2LinkContext.Instance);

        private M2LinkContext context;

        private UserRepository(M2LinkContext context)
        {
            this.context = context;
        }

        public void Add(User user)
        {
            CheckParameter("user", user);

            // Ensure that not any users contains the same mail address or username
            if (FindByUsername(user.Username) != null)
            {
                throw new ApplicationException(USERNAME_ALREADY_EXIST_MESSAGE);
            }
        
            // Add the user in the database
            context.Users.Add(user);
            context.SaveChanges();
        }

        public void Update(User user)
        {
            CheckParameter("user", user);

            // Notify the update of the user
            context.Entry(user).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(User user)
        {
            CheckParameter("user", user);

            // Ensure that the specified user exist
            if (FindByUsername(user.Username) == null)
            {
                throw new ApplicationException("L'utilisateur n'existe pas");
            }

            // Remove the user from the database
            context.Users.Remove(user);
            context.SaveChanges();
        }

        public User FindById(Guid id)
        {
            CheckParameter("id", id);
            return context.Users.FirstOrDefault(u => u.Id.Equals(id));
        }

        public User FindByUsername(string username)
        {
            CheckParameter("username", username);
            return context.Users.FirstOrDefault(u => u.Username.ToLower() == username.ToLower());
        }

        public User FindByMail(string mail)
        {
            CheckParameter("mail", mail);
            return context.Users.FirstOrDefault(u => u.Mail.ToLower() == mail.ToLower());
        }

        public IEnumerable<User> FindByGlobalSearch(string search)
        {
            CheckParameter("search", search);
            return context.Users
                    .Where(u => u.Username.ToLower().Contains(search.ToLower()) ||
                                u.FirstName.ToLower().Contains(search.ToLower()) ||
                                u.LastName.ToLower().Contains(search.ToLower()) ||
                                (u.FirstName + " " + u.LastName).ToLower().Contains(search.ToLower()));
        }

        public IEnumerable<User> GetAll()
        {
            return context.Users;
        }

        private void CheckParameter(string paramName, object paramValue)
        {
            // Ensure that the parameter is initialized
            if (paramValue == null)
            {
                throw new ApplicationException("Le paramètre "+ paramName + " ne peut pas être null");
            }
        }
    }
}