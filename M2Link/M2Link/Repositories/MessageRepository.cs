﻿using M2Link.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2Link.Entities
{
    // Singleton used to manage Messages Entities
    public class MessageRepository
    {
        public static readonly MessageRepository Instance = new MessageRepository(M2LinkContext.Instance);

        private M2LinkContext context;

        private MessageRepository(M2LinkContext context)
        {
            this.context = context;
        }

        public void Add(Message message)
        {
            CheckParameter("message", message);

            // Add the message in the database
            context.Messages.Add(message);
            context.SaveChanges();
        }

        public void Remove(Message message)
        {
            CheckParameter("message", message);

            // Remove the message from the database
            context.Messages.Remove(message);
            context.SaveChanges();
        }

        public Message FindById(Guid id)
        {
            CheckParameter("id", id);
            return context.Messages.FirstOrDefault(m => m.Id.Equals(id));
        }

        public IEnumerable<Message> FindByAuthor(User author)
        {
            CheckParameter("author", author);
            return context.Messages.Where(m => m.Author.Id.Equals(author.Id)).OrderByDescending(m => m.Date);
        }

        public IEnumerable<Message> GetFeed(User user)
        {
            CheckParameter("user", user);
            return user.Followings.SelectMany(u => u.Messages).Union(FindByAuthor(user)).OrderByDescending(m => m.Date);
        }

        public IEnumerable<Message> GetAll()
        {
            return context.Messages.OrderBy(m => m.Date);
        }

        private void CheckParameter(string paramName, object paramValue)
        {
            // Ensure that the parameter is initialized
            if (paramValue == null)
            {
                throw new ApplicationException("Le paramètre " + paramName + " ne peut pas être null");
            }
        }
    }
}