﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2LinkXamarin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UsersPage : ContentPage
	{
		public UsersPage ()
		{
			InitializeComponent ();

            // Get the list of the users
            WSHelper.WSUserClient.GetAll(WSHelper.AccessToken, "");
		}
	}
}