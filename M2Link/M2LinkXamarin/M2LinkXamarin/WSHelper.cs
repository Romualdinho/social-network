﻿using System.ServiceModel;

namespace M2LinkXamarin
{
    public class WSHelper
    {
        public static WSLoginClient WSLoginClient = new WSLoginClient(
            new BasicHttpBinding(),
            new EndpointAddress(AppConstants.WSServer + "/WebServices/WSLogin.svc"));

        public static WSRegisterClient WSRegisterClient = new WSRegisterClient(
            new BasicHttpBinding(),
            new EndpointAddress(AppConstants.WSServer + "/WebServices/WSRegister.svc"));

        public static WSUserClient WSUserClient = new WSUserClient(
            new BasicHttpBinding(),
            new EndpointAddress(AppConstants.WSServer + "/WebServices/WSUser.svc"));

        /*
        public static WSMessageClient WSMessageClient = new WSMessageClient(
            new BasicHttpBinding(),
            new EndpointAddress(AppConstants.WSServer + "/WebServices/WSMessage.svc"));
            */

        public static string AccessToken;
           
    }
}
