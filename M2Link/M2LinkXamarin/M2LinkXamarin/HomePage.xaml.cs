﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2LinkXamarin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();

            // Show all the messages feed of the authenticated user
            //messagesView.ItemsSource = WSHelper.WSMessageClient.GetFeed(WSHelper.AccessToken);
        }
	}
}