﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2LinkXamarin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();

            // Clear the access token
            WSHelper.AccessToken = "";

            // Using responsive WebView due to WCF services problems
            webView.Source = AppConstants.WSServer;
        }

        // DO NOT UNCOMMENT

        /*
        private void OnLogin(object sender, EventArgs e)
        {
            // Authenticate the user
            string token = WSHelper.WSLoginClient.Validate(username.Text, password.Text);

            if (token != null)
            {
                // Set the access token
                WSHelper.AccessToken = token;

                // Redirect to the home page
                Application.Current.MainPage = new MainPage();
            }
        }

        private void GoToSignUp(object sender, EventArgs e)
        {
            // Redirect to the register page
            Application.Current.MainPage = new RegisterPage();
        }*/
    }
}