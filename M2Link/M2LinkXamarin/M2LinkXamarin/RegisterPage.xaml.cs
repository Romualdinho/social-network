﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2LinkXamarin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			InitializeComponent ();
		}

        private void OnSignUp(object sender, EventArgs e)
        {
            // Register the user
            WSHelper.WSRegisterClient.Register(firstName.Text, lastName.Text, email.Text, username.Text, password.Text, confirmPassword.Text);

            // Redirecr to the login page
            Application.Current.MainPage = new LoginPage();
        }

        private void GoToLogin(object sender, EventArgs e)
        {
            // Redirect to the login page
            Application.Current.MainPage = new LoginPage();
        }
    }
}