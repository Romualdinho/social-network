﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace M2Link.Console.ExternalWebService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="UserModel", Namespace="http://tempuri.org/")]
    [System.SerializableAttribute()]
    public partial class UserModel : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private System.Guid IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FirstNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string LastNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string UsernameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PasswordField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public System.Guid Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string FirstName {
            get {
                return this.FirstNameField;
            }
            set {
                if ((object.ReferenceEquals(this.FirstNameField, value) != true)) {
                    this.FirstNameField = value;
                    this.RaisePropertyChanged("FirstName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string LastName {
            get {
                return this.LastNameField;
            }
            set {
                if ((object.ReferenceEquals(this.LastNameField, value) != true)) {
                    this.LastNameField = value;
                    this.RaisePropertyChanged("LastName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string Mail {
            get {
                return this.MailField;
            }
            set {
                if ((object.ReferenceEquals(this.MailField, value) != true)) {
                    this.MailField = value;
                    this.RaisePropertyChanged("Mail");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string Username {
            get {
                return this.UsernameField;
            }
            set {
                if ((object.ReferenceEquals(this.UsernameField, value) != true)) {
                    this.UsernameField = value;
                    this.RaisePropertyChanged("Username");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string Password {
            get {
                return this.PasswordField;
            }
            set {
                if ((object.ReferenceEquals(this.PasswordField, value) != true)) {
                    this.PasswordField = value;
                    this.RaisePropertyChanged("Password");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ExternalWebService.WSLoginSoap")]
    public interface WSLoginSoap {
        
        // CODEGEN : La génération du contrat de message depuis le nom d'élément login de l'espace de noms http://tempuri.org/ n'est pas marqué nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Validate", ReplyAction="*")]
        M2Link.Console.ExternalWebService.ValidateResponse Validate(M2Link.Console.ExternalWebService.ValidateRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Validate", ReplyAction="*")]
        System.Threading.Tasks.Task<M2Link.Console.ExternalWebService.ValidateResponse> ValidateAsync(M2Link.Console.ExternalWebService.ValidateRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ValidateRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Validate", Namespace="http://tempuri.org/", Order=0)]
        public M2Link.Console.ExternalWebService.ValidateRequestBody Body;
        
        public ValidateRequest() {
        }
        
        public ValidateRequest(M2Link.Console.ExternalWebService.ValidateRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class ValidateRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string login;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string password;
        
        public ValidateRequestBody() {
        }
        
        public ValidateRequestBody(string login, string password) {
            this.login = login;
            this.password = password;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ValidateResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ValidateResponse", Namespace="http://tempuri.org/", Order=0)]
        public M2Link.Console.ExternalWebService.ValidateResponseBody Body;
        
        public ValidateResponse() {
        }
        
        public ValidateResponse(M2Link.Console.ExternalWebService.ValidateResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class ValidateResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public M2Link.Console.ExternalWebService.UserModel ValidateResult;
        
        public ValidateResponseBody() {
        }
        
        public ValidateResponseBody(M2Link.Console.ExternalWebService.UserModel ValidateResult) {
            this.ValidateResult = ValidateResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WSLoginSoapChannel : M2Link.Console.ExternalWebService.WSLoginSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WSLoginSoapClient : System.ServiceModel.ClientBase<M2Link.Console.ExternalWebService.WSLoginSoap>, M2Link.Console.ExternalWebService.WSLoginSoap {
        
        public WSLoginSoapClient() {
        }
        
        public WSLoginSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WSLoginSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSLoginSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSLoginSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        M2Link.Console.ExternalWebService.ValidateResponse M2Link.Console.ExternalWebService.WSLoginSoap.Validate(M2Link.Console.ExternalWebService.ValidateRequest request) {
            return base.Channel.Validate(request);
        }
        
        public M2Link.Console.ExternalWebService.UserModel Validate(string login, string password) {
            M2Link.Console.ExternalWebService.ValidateRequest inValue = new M2Link.Console.ExternalWebService.ValidateRequest();
            inValue.Body = new M2Link.Console.ExternalWebService.ValidateRequestBody();
            inValue.Body.login = login;
            inValue.Body.password = password;
            M2Link.Console.ExternalWebService.ValidateResponse retVal = ((M2Link.Console.ExternalWebService.WSLoginSoap)(this)).Validate(inValue);
            return retVal.Body.ValidateResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<M2Link.Console.ExternalWebService.ValidateResponse> M2Link.Console.ExternalWebService.WSLoginSoap.ValidateAsync(M2Link.Console.ExternalWebService.ValidateRequest request) {
            return base.Channel.ValidateAsync(request);
        }
        
        public System.Threading.Tasks.Task<M2Link.Console.ExternalWebService.ValidateResponse> ValidateAsync(string login, string password) {
            M2Link.Console.ExternalWebService.ValidateRequest inValue = new M2Link.Console.ExternalWebService.ValidateRequest();
            inValue.Body = new M2Link.Console.ExternalWebService.ValidateRequestBody();
            inValue.Body.login = login;
            inValue.Body.password = password;
            return ((M2Link.Console.ExternalWebService.WSLoginSoap)(this)).ValidateAsync(inValue);
        }
    }
}
