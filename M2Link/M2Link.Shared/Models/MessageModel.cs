﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace M2Link.Shared.Models
{
    public class MessageModel
    {
        public Guid Id { get; set; }
        public UserModel Author { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public string Image { get; set; }

        public override bool Equals(object obj)
        {
            var model = obj as MessageModel;
            return model != null &&
                   Id.Equals(model.Id);
        }

        public override int GetHashCode()
        {
            return 2108858624 + EqualityComparer<Guid>.Default.GetHashCode(Id);
        }
    }
}