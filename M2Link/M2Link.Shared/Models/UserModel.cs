﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link.Shared.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mail { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }
        public ICollection<MessageModel> Messages { get; set; }
        public ICollection<UserModel> Followers { get; set; }
        public ICollection<UserModel> Followings { get; set; }

        public UserModel()
        {
            Messages = new List<MessageModel>();
            Followers = new List<UserModel>();
            Followings = new List<UserModel>();
        }

        public override bool Equals(object obj)
        {
            var model = obj as UserModel;
            return model != null &&
                   Id.Equals(model.Id);
        }

        public override int GetHashCode()
        {
            return 2108858624 + EqualityComparer<Guid>.Default.GetHashCode(Id);
        }
    }
}